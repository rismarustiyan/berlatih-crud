@extends('adminlte/master')
@section('content')
<div class="card-header">
    <h3 class="card-title">Form Cast</h3>
  </div>
<form role="form" action="/cast" method="POST">
    @csrf
    <div class="card-body">
        <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" id="nama" name="nama" placeholder="Enter nama">
        </div>
        <div class="form-group">
        <label>Umur</label>
        <input type="text" class="form-control" id="umur" name="umur" placeholder="Enter umur">
        </div>
      <div class="form-group">
        <label>Bio</label>
        <textarea type="text" class="form-control" rows="3" id="bio" name="bio" placeholder="Enter Bio"></textarea>
      </div>

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
@endsection